#!/bin/sh 
source ./variables

/usr/bin/ip link set $LINK down
/usr/bin/ip addr add 0.0.0.0 dev $LINK
/usr/bin/ip link set $LINK promisc on

/usr/bin/ip tuntap add $TAP mode tap
/usr/bin/ip addr add 0.0.0.0 dev $TAP
/usr/bin/ip link set $TAP promisc on

/usr/bin/ip link add $BRIDGE type bridge
/usr/bin/ip link set $LINK master $BRIDGE
/usr/bin/ip link set $TAP master $BRIDGE

/usr/bin/ip link set $LINK up
/usr/bin/ip link set $TAP up
/usr/bin/ip link set $BRIDGE up
