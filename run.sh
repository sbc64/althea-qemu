#!/usr/bin/env bash

source ./variables
# -netdev tap,id=network0,ifname=tap0,script=no,downscript=no,vhost=on -device virtio-net,netdev=network0
# same as: 
#-nic tap,ifname=tap0,script=no,downscript=no,vhost=on,model=virtio-net

# virtio-net-device
# In addition, networking performance can be improved by assigning virtual machines 
#a virtio network device rather than the default emulation of an e1000 NIC. 
sudo ./up-qemu.sh
sudo qemu-system-x86_64 \
  -enable-kvm \
  -m 1024 \
  -drive file=althea.qcow2,media=disk,if=virtio \
  -nographic \
  -netdev tap,\
    id=tap,\
    ifname=$TAP,\
    br=$BRIDGE,\
    script=no,\
    dowscript=no,\
    vhost=on,\
    model=virtio-net,\
    hostfwd=tcp::8022-:22
sudo ./down-qemu.sh
