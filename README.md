#### Before

Make sure that you have KVM enabled/installed and that virtualisation is turned on in your BIOS

#### 1. Download x86-64 image from:
[https://github.com/althea-mesh/althea-firmware/releases](https://github.com/althea-mesh/althea-firmware/releases)

#### 2. Prepare the image file

Use qemu-img to create an image file.

```
qemu-img dd if=openwrt-x86-64-combined-squashfs.img of=althea.raw bs=4M
qemu-img convert althea.raw -O qcow2 althea.qcow2
qemu-img resize althea.qcow2 +500M
```

#### 3. Start QEMU image

```
qemu-system-x86_64 -enable-kvm -m 2048 -nic user,model=virtio -drive file=althea.qcow2,media=disk,if=virtio -cdrom openwrt-x86-64-combined-squashfs.img -sdl
```
