#!/bin/sh 
source ./variables

/usr/bin/ip link set $LINK down
/usr/bin/ip link set $TAP down
/usr/bin/ip link set $BRIDGE down

/usr/bin/ip link del dev $TAP
/usr/bin/ip link del dev $BRIDGE

/usr/bin/ip addr flush dev $LINK
/usr/bin/ip link set $LINK promisc off
/usr/bin/ip link set $LINK up
